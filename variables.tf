variable "is_desenv" {
  description = "Teste de variable"
  type        = string
  default     = "dev"
}

variable "is_production" {
  description = "Teste de variable"
  type        = string
  default     = "prod"

}

variable "aws_key_pub" {
  description = "Chave publica"
  type        = string
}