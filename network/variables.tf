variable "cidr_vpc" {
  description = "Cidir da VPC"
  type        = string
}

variable "cidr_subnet_a" {
  description = "Cidir da Suba"
  type        = string
}

variable "cidr_subnet_b" {
  description = "Cidir da Sub"
  type        = string
}

variable "environment" {
  description = "Ambiente que vai ser aplicado"
  type        = string
}

